package main

import (
	"fmt"
	"log"
	t "time"
	bs "bitbucket.org/ais2017/bank.support.fri/bank.support/business"
)

func main() {
	fmt.Printf("Hello, world.\n")

	e, err := bs.NewEmployee("Bond", "James", bs.SupportEmployee)
	if err != nil {
		log.Fatal(err)
	}

	client := bs.NewClient("Bond", "James Bond")
	ca, err := bs.NewClientApplication(client, "Zorz", t.Now())
	for i := 0; i < 5; i++ {
		req, err := bs.NewRequest(i, *ca, e)
		if err != nil {
			fmt.Printf("%s\n", err)
			continue
		}
		e.AddRequest(req)
	}

	for i, req := range e.GetAssignedRequests() {
		fmt.Printf("%d) %d\n", i, req.Id())
	}
	fmt.Printf("\n")
	e.RemoveRequest(1)
	for i, req := range e.GetAssignedRequests() {
		fmt.Printf("%d) %d\n", i, req.Id())
	}
	// p := NewClient("Bond", "James Bond")
	fmt.Printf("%s %s %d\n", e.FirstName(), e.LastName(), e.Role())
}
