package business

import (
	"errors"
)

// Constants for role in Person struct.
type Role int

const (
	SupportEmployee Role = iota
	BankEmployee
	Client
)

// type AnyPerson interface {
// FirstName() string
// LastName() string
// Role()
// }

// TODO: how to call interface? Should it contain all methods?
// TODO: try to make Person private to disable creation without NewPerson

/*
Person struct, must have role equal to Client if not embedded.

Person should be created ONLY via NewPerson contructor!
*/
type Person struct {
	firstName string
	lastName  string
	role      Role
}

type EmployeeRequest interface {
	Id() int
}

/*
Employee struct, must have role equal to SupportEmployee or BankEmployee if not embedded.

Employee should be created ONLY via NewEmployee contructor!
*/
type Employee struct {
	Person
	assignedRequests []EmployeeRequest // TODO: read more about slices: https://blog.golang.org/go-slices-usage-and-internals
}

func NewClient(fName, sName string) *Person {
	return &Person{fName, sName, Client}
}

func NewEmployee(fName, sName string, role Role) (*Employee, error) {
	if role != BankEmployee && role != SupportEmployee {
		return nil, errors.New("Employee: Person is not employee")
	}

	e := new(Employee)
	e.firstName = fName
	e.lastName = sName
	e.role = role

	return e, nil
}

func (p *Person) FirstName() string {
	return p.firstName
}

func (p *Person) LastName() string {
	return p.lastName
}

func (p *Person) Role() Role {
	return p.role
}

// TODO: check for using pointers!
// TODO: check for existence and return error
func (e *Employee) AddRequest(req EmployeeRequest) {
	e.assignedRequests = append(e.assignedRequests, req)
}

// TODO: is it normal to return error on remove failure?
func (e *Employee) RemoveRequest(rid int) error {
	ar := e.assignedRequests
	for i := range ar {
		if ar[i].Id() == rid {
			ar[i] = ar[len(ar)-1]
			ar = ar[:len(ar)-1]
			e.assignedRequests = ar
			return nil
		}
	}
	return errors.New("Employee: Request not found")
}

// TODO: may be return copy?
func (e *Employee) GetAssignedRequests() []EmployeeRequest {
	return e.assignedRequests
}
