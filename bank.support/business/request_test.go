package business

import (
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	t "testing"
	"time"
)

func TestRequestStatusFromString(t *t.T) {
	testCases := []struct {
		statusString string
		statusGot    RequestStatus
		ok           bool
	}{
		{"InProgressReq", InProgressReq, true},
		{"RejectedReq", RejectedReq, true},
		{"AcceptedReq", AcceptedReq, true},
		{"ReassignedReq", ReassignedReq, true},
		{"ReassignedReq123", -1, false},
		{"Zorz", -1, false},
	}
	assert := assert.New(t)

	for _, v := range testCases {
		st, ok := RequestStatusFromString(v.statusString)
		if !v.ok {
			assert.False(ok)
		} else {
			assert.True(ok)
			assert.Equal(st, v.statusGot)
		}
	}

}

func TestRedirectionStatusFromString(t *t.T) {
	testCases := []struct {
		statusString string
		statusGot    RedirectionStatus
		ok           bool
	}{
		{"InProgress", InProgress, true},
		{"Rejected", Rejected, true},
		{"Accepted", Accepted, true},
		{"ReassignedReq123", -1, false},
		{"Zorz", -1, false},
	}
	assert := assert.New(t)

	for _, v := range testCases {
		st, ok := RedirectionStatusFromString(v.statusString)
		if !v.ok {
			assert.False(ok)
		} else {
			assert.True(ok)
			assert.Equal(st, v.statusGot)
		}
	}

}

func TestNewClientApplication(t *t.T) {
	testCases := []struct {
		p        *Person
		tm       time.Time
		contents string
		hasError bool
	}{
		{NewClient("James", "Bond"), time.Date(2009, time.November, 10, 23, 0, 0, 0, time.UTC), "Some contents", false},
		{NewClient("Ululu", "NoUlulu"), time.Date(2000, time.November, 10, 23, 1, 0, 0, time.UTC), "Here are contents", false},
		{&Person{"Zorz", "NotZorz", SupportEmployee}, time.Date(2009, time.November, 10, 23, 0, 0, 0, time.UTC), "Other contents", true},
	}

	assert := assert.New(t)

	for _, v := range testCases {
		ca, err := NewClientApplication(v.p, v.contents, v.tm)

		if v.hasError {
			assert.NotNil(err)
			continue
		}

		assert.NotNil(ca)
		assert.Nil(err)

		assert.Equal(v.contents, ca.Content())
		assert.Equal(v.tm, ca.Date())
		assert.Equal(*v.p, *ca.Client())
	}
}

func TestNewRedirection(t *t.T) {
	testCases := []struct {
		createDate time.Time
		openMsg    string
		assignee   *Employee
		hasError   bool
	}{
		{time.Date(2009, time.November, 10, 23, 0, 0, 0, time.UTC), "Zorz",
			&Employee{Person{"FN", "SN", SupportEmployee}, []EmployeeRequest{}}, true},
		{time.Date(2009, time.November, 10, 23, 0, 0, 0, time.UTC), "No Zorz",
			&Employee{Person{"FN1", "SN2", BankEmployee}, []EmployeeRequest{}}, false},
	}

	assert := assert.New(t)

	for _, v := range testCases {
		rd, err := NewRedirection(v.createDate, v.openMsg, v.assignee)

		if v.hasError {
			assert.NotNil(err)
			continue
		}

		assert.NotNil(rd)
		assert.Nil(err)

		assert.Equal(InProgress, rd.Status())
		assert.Equal(v.createDate, rd.CreateDate())
		assert.Equal(v.openMsg, rd.OpenMessage())
		assert.Equal(*v.assignee, *rd.Assignee())
	}
}

func TestCloseRedirection(t *t.T) {
	assert := assert.New(t)
	require := require.New(t)

	tm := time.Date(2009, time.November, 10, 23, 0, 0, 0, time.UTC)
	emp := Employee{Person{"FN", "SN", BankEmployee}, []EmployeeRequest{}}

	testCases := []struct {
		closeDate    time.Time
		closeMessage string
		status       RedirectionStatus
	}{
		{tm, "My Zorz", Rejected},
		{tm, "Not my Zorz", Accepted},
	}

	for _, v := range testCases {
		rd, err := NewRedirection(tm, "Zorz", &emp)

		require.Nil(err)
		if v.status == Accepted {
			assert.Nil(rd.Accept(v.closeMessage, v.closeDate))
			assert.NotNil(rd.Accept(v.closeMessage, v.closeDate))
			assert.NotNil(rd.Reject(v.closeMessage, v.closeDate))
		} else {
			assert.Nil(rd.Reject(v.closeMessage, v.closeDate))
			assert.NotNil(rd.Accept(v.closeMessage, v.closeDate))
			assert.NotNil(rd.Reject(v.closeMessage, v.closeDate))
		}

		assert.Equal(v.status, rd.Status())
		assert.Equal(v.closeMessage, rd.CloseMessage())
		assert.Equal(v.closeDate, rd.CloseDate())
	}
}

func TestNewRequest(t *t.T) {
	assert := assert.New(t)
	require := require.New(t)

	tm := time.Date(2009, time.November, 10, 23, 0, 0, 0, time.UTC)
	be := Employee{Person{"F1", "S1", BankEmployee}, []EmployeeRequest{}}
	se := Employee{Person{"F2", "S2", SupportEmployee}, []EmployeeRequest{}}
	cl := NewClient("F3", "S3")
	ca, err := NewClientApplication(cl, "zorz", tm)
	require.Nil(err)

	// NewRequest(id int, ca ClientApplication, se *Employee)
	testCases := []struct {
		id       int
		ca       ClientApplication
		se       *Employee
		hasError bool
	}{
		{-1, *ca, &be, true},
		{-1, *ca, &se, true},
		{0, *ca, &se, false},
		{1, *ca, &se, false},
		{1000, *ca, &be, true},
	}

	for _, v := range testCases {
		rq, err := NewRequest(v.id, v.ca, v.se)

		if v.hasError {
			assert.NotNil(err)
			continue
		}

		assert.NotNil(rq)
		assert.Nil(err)

		assert.Equal(v.id, rq.Id())
		assert.Equal(v.ca, rq.Application())
		assert.Equal(*v.se, *rq.Assignee())
		assert.Equal(false, rq.IsReassigned())
		assert.Equal(InProgressReq, rq.Status())
	}
}

func TestCloseRequest(t *t.T) {
	assert := assert.New(t)
	require := require.New(t)

	tm := time.Date(2007, time.November, 10, 23, 0, 0, 0, time.UTC)
	se := Employee{Person{"Just", "SEmployee", SupportEmployee}, []EmployeeRequest{}}
	be := Employee{Person{"Just", "BEmployee", BankEmployee}, []EmployeeRequest{}}
	ca, err := NewClientApplication(NewClient("Just", "Client"), "zorz", tm)
	require.Nil(err)
	rd, err := NewRedirection(tm, "Zorz2", &be)
	require.Nil(err)

	testCases := []struct {
		msg    string
		status RequestStatus
	}{
		{"Zorz", RejectedReq},
		{"NotZorz", RejectedReq},
		{"Ululu", AcceptedReq},
		{"Nope", AcceptedReq},
		{"1Nope", ReassignedReq},
	}

	for _, v := range testCases {
		rq, err := NewRequest(1, *ca, &se)
		require.Nil(err)

		if v.status == AcceptedReq {
			assert.Nil(rq.CloseWithSuccess(v.msg))
			assert.NotNil(rq.CloseWithSuccess(v.msg))
			assert.NotNil(rq.Reject(v.msg))

			assert.Equal(v.msg, rq.Result())
			assert.Equal(v.status, rq.Status())

		} else if v.status == RejectedReq {
			assert.Nil(rq.Reject(v.msg))
			assert.NotNil(rq.CloseWithSuccess(v.msg))
			assert.NotNil(rq.Reject(v.msg))

			assert.Equal(v.msg, rq.Result())
			assert.Equal(v.status, rq.Status())
		} else {
			rq.Redirect(*rd)
			assert.NotNil(rq.CloseWithSuccess(v.msg))
			assert.NotNil(rq.Reject(v.msg))
		}
	}
}

func TestRequestRedirections(t *t.T) {
	assert := assert.New(t)
	require := require.New(t)

	tm := time.Date(2007, time.November, 10, 23, 0, 0, 0, time.UTC)
	se := Employee{Person{"Just", "SEmployee", SupportEmployee}, []EmployeeRequest{}}
	be := Employee{Person{"F1", "S1", BankEmployee}, []EmployeeRequest{}}
	ca, err := NewClientApplication(NewClient("Just", "Client"), "zorz", tm)
	require.Nil(err)

	rq1, err1 := NewRequest(1, *ca, &se)
	rq2, err2 := NewRequest(2, *ca, &se)

	require.Nil(err1)
	require.Nil(err2)
	require.Nil(rq1.LastRedirection())
	require.Nil(rq2.LastRedirection())

	rd1, err1 := NewRedirection(tm, "Zorz1", &be)
	rd2, err2 := NewRedirection(tm, "Zorz2", &be)
	require.Nil(err1)
	require.Nil(err2)

	err1 = rq1.Redirect(*rd1)
	err2 = rq2.Redirect(*rd2)

	assert.Nil(err1)
	assert.Nil(err2)

	assert.Equal(true, rq1.IsReassigned())
	assert.Equal(ReassignedReq, rq1.Status())
	assert.Equal(*rd1, *rq1.LastRedirection())

	err = rq1.Redirect(*rd2)
	assert.NotNil(err)

	err = rq1.ReturnFromRedirection()
	assert.Nil(err)
	assert.Equal(false, rq1.IsReassigned())
	assert.Equal(InProgressReq, rq1.Status())

	err = rq1.ReturnFromRedirection()
	assert.NotNil(err)

	err = rq1.Redirect(*rd2)
	assert.Nil(err)

	assert.Equal(true, rq1.IsReassigned())
	assert.Equal(ReassignedReq, rq1.Status())
	assert.Equal(*rd2, *rq1.LastRedirection())
}
