package business

import (
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	t "testing"
)

func TestNewClient(t *t.T) {
	assert := assert.New(t)

	p := NewClient("James", "Bond")
	assert.Equal(p.FirstName(), "James")
	assert.Equal(p.LastName(), "Bond")
	assert.Equal(p.Role(), Client)
}

func TestNewEmployee(t *t.T) {
	testCases := []struct {
		fname    string
		lname    string
		role     Role
		hasError bool
	}{
		{"Jack", "Zorz", BankEmployee, false},
		{"James", "Bond", SupportEmployee, false},
		{"James", "Bond", Client, true},
		{"1", "2", SupportEmployee, false},
	}

	assert := assert.New(t)

	for _, v := range testCases {
		p, err := NewEmployee(v.fname, v.lname, v.role)

		if v.hasError {
			assert.NotNil(err)
			continue
		}
		assert.Nil(err)
		assert.Equal(p.FirstName(), v.fname)
		assert.Equal(p.LastName(), v.lname)
		assert.Equal(p.Role(), v.role)
	}
}

type testRequest struct {
	id int
}

func (tr *testRequest) Id() int {
	return tr.id
}

func TestEmployeeRequests(t *t.T) {
	require := require.New(t)
	assert := assert.New(t)

	se, err := NewEmployee("Support", "The greatest", SupportEmployee)
	require.Nil(err)
	be, err := NewEmployee("Banker", "Bank", BankEmployee)
	require.Nil(err)

	require.Equal([]EmployeeRequest(nil), se.GetAssignedRequests())
	require.Equal([]EmployeeRequest(nil), be.GetAssignedRequests())

	for i := 0; i < 10; i++ {
		se.AddRequest(&testRequest{i})
		be.AddRequest(&testRequest{i})
	}

	se_ar := se.GetAssignedRequests()
	be_ar := be.GetAssignedRequests()
	for i := 0; i < 10; i++ {
		assert.Equal(i, se_ar[i].Id())
		assert.Equal(i, be_ar[i].Id())
	}

	err = se.RemoveRequest(7)
	require.Nil(err)
	err = be.RemoveRequest(7)
	require.Nil(err)

	err = se.RemoveRequest(7)
	require.NotNil(err)
	err = be.RemoveRequest(7)
	require.NotNil(err)

	se_ar = se.GetAssignedRequests()
	be_ar = be.GetAssignedRequests()
	assert.Equal(9, len(se_ar))
	assert.Equal(9, len(be_ar))

	ids := []int{0, 1, 2, 3, 4, 5, 6, 8, 9}
	find := func(ar []EmployeeRequest, elem int) bool {
		for _, er := range ar {
			if er.Id() == elem {
				return true
			}
		}
		return false
	}

	for _, elem := range ids {
		assert.Equal(true, find(se_ar, elem))
		assert.Equal(true, find(be_ar, elem))
	}

	assert.Equal(false, find(se_ar, 11))
	assert.Equal(false, find(be_ar, 11))
	assert.Equal(false, find(se_ar, 7))
	assert.Equal(false, find(be_ar, 7))
}
