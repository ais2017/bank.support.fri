package business

import (
	"errors"
	t "time"
)

//go:generate stringer -type=RedirectionStatus,RequestStatus -output=const_string.go
type RedirectionStatus int

const (
	InProgress RedirectionStatus = iota
	Rejected
	Accepted
)

func RedirectionStatusFromString(status string) (RedirectionStatus, bool) {
	res, ok := map[string]RedirectionStatus{
		"InProgress": InProgress,
		"Rejected":   Rejected,
		"Accepted":   Accepted,
	}[status]
	return res, ok
}

type RequestStatus int

const (
	InProgressReq RequestStatus = iota
	RejectedReq
	AcceptedReq
	ReassignedReq
)

func RequestStatusFromString(status string) (RequestStatus, bool) {
	res, ok := map[string]RequestStatus{
		"InProgressReq": InProgressReq,
		"RejectedReq":   RejectedReq,
		"AcceptedReq":   AcceptedReq,
		"ReassignedReq": ReassignedReq,
	}[status]
	return res, ok
}

type Request struct {
	id         int
	ca         ClientApplication
	status     RequestStatus
	result     string
	assignee   *Employee
	reassigned bool
	history    []Redirection
}

type ClientApplication struct {
	client  *Person
	content string
	date    t.Time
}

type Redirection struct {
	createDate   t.Time
	closeDate    t.Time
	openMessage  string
	closeMessage string
	assignee     *Employee
	status       RedirectionStatus
}

// ---------------------------------------------------------

func NewClientApplication(client *Person, content string, date t.Time) (*ClientApplication, error) {
	if client.Role() != Client {
		return nil, errors.New("CA: person has not Client role")
	}

	return &ClientApplication{client, content, date}, nil
}

func (ca *ClientApplication) Client() *Person {
	return ca.client
}

func (ca *ClientApplication) Content() string {
	return ca.content
}

func (ca *ClientApplication) Date() t.Time {
	return ca.date
}

// ---------------------------------------------------------

func NewRequest(id int, ca ClientApplication, se *Employee) (*Request, error) {
	if id < 0 {
		return nil, errors.New("Req: Wrong id")
	}

	if se.Role() != SupportEmployee {
		return nil, errors.New("Req: Employee must be SupportEmployee only")
	}

	r := new(Request)
	r.id = id
	r.ca = ca
	r.assignee = se
	r.status = InProgressReq
	r.reassigned = false
	return r, nil
}

func (req *Request) CloseWithSuccess(msg string) error {
	if req.status != InProgressReq {
		return errors.New("Req: Can not close request not in progress state")
	}
	req.status = AcceptedReq
	req.result = msg
	return nil
}

func (req *Request) Reject(msg string) error {
	if req.status != InProgressReq {
		return errors.New("Req: Can not close request not in progress state")
	}

	req.status = RejectedReq
	req.result = msg
	return nil
}

func (req *Request) Redirect(rd Redirection) error {
	if req.status != InProgressReq {
		return errors.New("Req: Can redirect only request that in progress")
	}
	req.history = append(req.history, rd)
	req.reassigned = true
	req.status = ReassignedReq
	return nil
}

func (req *Request) LastRedirection() *Redirection {
	if len(req.history) == 0 {
		return nil
	}
	return &req.history[len(req.history)-1]
}

func (req *Request) ReturnFromRedirection() error {
	if !req.IsReassigned() {
		return errors.New("Req: Should be redirected")
	}
	req.status = InProgressReq
	req.reassigned = false
	return nil
}

func (req *Request) Id() int {
	return req.id
}

func (req *Request) Application() ClientApplication {
	return req.ca
}

func (req *Request) IsReassigned() bool {
	return req.reassigned
}

func (req *Request) Status() RequestStatus {
	return req.status
}

func (req *Request) Assignee() *Employee {
	return req.assignee
}

func (req *Request) Result() string {
	return req.result
}

// ---------------------------------------------------------

func NewRedirection(createDate t.Time, openMsg string, assignee *Employee) (*Redirection, error) {
	if assignee.Role() != BankEmployee {
		return nil, errors.New("Redirection: assignee must be BankEmployee")
	}

	rd := new(Redirection)
	rd.createDate = createDate
	rd.openMessage = openMsg
	rd.assignee = assignee
	rd.status = InProgress
	return rd, nil
}

// TODO: change time to Now.
// NOTE: need to know how to mock, otherwise it is impossible to test
func (rd *Redirection) Accept(msg string, date t.Time) error {
	if rd.status != InProgress {
		return errors.New("Redirection: Can't accept Redirection not in progress state")
	}

	rd.status = Accepted
	rd.closeMessage = msg
	rd.closeDate = date

	return nil
}

func (rd *Redirection) Reject(msg string, date t.Time) error {
	if rd.status != InProgress {
		return errors.New("Redirection: Can't reject Redirection not in progress state")
	}

	rd.status = Rejected
	rd.closeMessage = msg
	rd.closeDate = date

	return nil
}

func (rd *Redirection) OpenMessage() string {
	return rd.openMessage
}

func (rd *Redirection) CloseMessage() string {
	return rd.closeMessage
}

func (rd *Redirection) CreateDate() t.Time {
	return rd.createDate
}

func (rd *Redirection) CloseDate() t.Time {
	return rd.closeDate
}

func (rd *Redirection) Assignee() *Employee {
	return rd.assignee
}

func (rd *Redirection) Status() RedirectionStatus {
	return rd.status
}
