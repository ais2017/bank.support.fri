package controller

import (
	b "bitbucket.org/ais2017/bank.support.fri/bank.support/business"
	"errors"
	t "time"
)

// Support system storage interface
type Storage interface {
	LeastBusySupportEmployee() *b.Employee
	GetEmployee(eid int) (*b.Employee, error)

	AllocateRequestID() int // allocated id for new request
	StoreRequest(rq *b.Request) error
	GetRequest(rid int) (*b.Request, error)
	UpdateRequest(*b.Request) error
}

// Interface to system that stores any data about Clients
type ClientSystem interface {
	GetClientByName(fName, lName string) *b.Person // returns nil in case of any problem
}

// Interface to system that stores any data about Employees
type EmployeeSystem interface {
	// getSupportEmployees() []int // id of Employee
	// getEmployeeByID(id int) *PersonDTO
	GetEmployeeID(fName, sName string) int // returns -1 in case of any problem
}

// Controller of support system
type Controller struct {
	st        Storage
	empSys    EmployeeSystem
	clientSys ClientSystem
}

// NOTE: DTO should be used to parse data from server

type PersonDTO struct {
	FirstName string
	LastName  string
	Role      b.Role
}

// Create request from ca and autoassign it to the least busy SupportEmployee
// Returns; id of created request
func (a *Controller) AutoCreateRequest(caContent string, client PersonDTO) (int, error) {
	cl := a.clientSys.GetClientByName(client.FirstName, client.LastName)
	if cl == nil {
		return -1, errors.New("Controller: could not add/find client from external system")
	}

	// TODO: may be it also should be stored at storage?
	ca, err := b.NewClientApplication(cl, caContent, t.Now())
	if err != nil {
		return -1, err
	}

	leastBusyEmp := a.st.LeastBusySupportEmployee()
	if leastBusyEmp == nil {
		return -1, errors.New("AutoCreateRequest: there are no employees")
	}

	reqID := a.st.AllocateRequestID()
	rq, err := b.NewRequest(reqID, *ca, leastBusyEmp)
	if err != nil {
		return -1, err
	}

	a.st.StoreRequest(rq)

	return rq.Id(), nil
}

// Close request by id with RequestStatus and msg
func (a *Controller) closeRequestCommon(rid int, msg string, reqStatus b.RequestStatus) error {
	req, err := a.st.GetRequest(rid)
	if err != nil {
		return err
	}

	if reqStatus == b.AcceptedReq {
		err = req.CloseWithSuccess(msg)
	} else if reqStatus == b.RejectedReq {
		err = req.Reject(msg)
	} else {
		err = errors.New("closeRequestCommon: unknown status")
	}
	if err != nil {
		return err
	}

	err = a.st.UpdateRequest(req)
	if err != nil {
		return err
	}

	return nil
}

func (a *Controller) CloseRequestWithSuccess(rid int, msg string) error {
	return a.closeRequestCommon(rid, msg, b.AcceptedReq)
}

func (a *Controller) CloseRequestWithError(rid int, msg string) error {
	return a.closeRequestCommon(rid, msg, b.RejectedReq)
}

// Get open requests for employee
func (a *Controller) GetOpenRequests(fName, sName string) ([]*b.Request, error) {
	empID := a.empSys.GetEmployeeID(fName, sName)
	if empID == -1 {
		return nil, errors.New("GetOpenRequests: employee not found")
	}

	emp, err := a.st.GetEmployee(empID)
	if err != nil {
		return nil, err
	}

	rqs := []*b.Request{}
	for _, rq := range emp.GetAssignedRequests() {
		rqNew, ok := rq.(*b.Request)
		if !ok {
			return nil, errors.New("GetOpenRequests: unexpected Requests type")
		}
		rqs = append(rqs, rqNew)
	}

	return rqs, nil
}

// Redirect Request from SupportEmployee to BankEmployee by id
func (a *Controller) RedirectRequest(rid int, beFName string, beSName string, msg string) error {
	req, err := a.st.GetRequest(rid)
	if err != nil {
		return err
	}

	empID := a.empSys.GetEmployeeID(beFName, beSName)
	if empID == -1 {
		return errors.New("RedirectRequest: employee not found")
	}

	be, err := a.st.GetEmployee(empID)
	if err != nil {
		return err
	}

	rd, err := b.NewRedirection(t.Now(), msg, be)
	if err != nil {
		return err
	}

	err = req.Redirect(*rd)

	err = a.st.UpdateRequest(req)
	if err != nil {
		return err
	}

	return nil
}

// Return Request from BankEmployee to SupportEmployee
func (a *Controller) CloseRedirection(rid int, msg string) error {
	req, err := a.st.GetRequest(rid)
	if err != nil {
		return err
	}

	err = req.ReturnFromRedirection()
	if err != nil {
		return err
	}

	err = a.st.UpdateRequest(req)
	if err != nil {
		return err
	}

	return nil
}
