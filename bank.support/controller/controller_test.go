package controller

import (
	b "bitbucket.org/ais2017/bank.support.fri/bank.support/business"
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	t "testing"
	"time"
)

type SimpleStorage struct {
	lbsEmployee   *b.Employee
	currRequestID int
	Err           bool
	employee      *b.Employee
	request       *b.Request
}

func (ss *SimpleStorage) SetEmployee(emp *b.Employee) {
	ss.employee = emp
}

func (ss *SimpleStorage) SetLbsEmployee(lbse *b.Employee) {
	ss.lbsEmployee = lbse
}

func (ss *SimpleStorage) AddEmployeeOpenRequest(openReq *b.Request) {
	if ss.employee != nil {
		ss.employee.AddRequest(openReq)
	}
}

func (ss *SimpleStorage) RemoveRequest() {
	ss.request = nil
}

// ------------- < Storage interface implementaion > --------------- //
func (ss *SimpleStorage) LeastBusySupportEmployee() *b.Employee {
	return ss.lbsEmployee
}

func (ss *SimpleStorage) StoreRequest(rq *b.Request) error {
	return nil
}

func (ss *SimpleStorage) AllocateRequestID() int {
	return 1
}

func (ss *SimpleStorage) GetRequest(rid int) (*b.Request, error) {
	if rid == 7 {
		return nil, errors.New("GetRequest: not found")
	}

	if ss.request != nil {
		return ss.request, nil
	}

	tm := time.Date(2007, time.November, 10, 23, 0, 0, 0, time.UTC)
	se, err := b.NewEmployee("Jack", "Daniels", b.SupportEmployee)
	if err != nil {
		return nil, err
	}

	ca, err := b.NewClientApplication(b.NewClient("Just", "Client"), "zorz", tm)
	if err != nil {
		return nil, err
	}

	rq, err := b.NewRequest(1, *ca, se)
	return rq, err
}

func (ss *SimpleStorage) UpdateRequest(rq *b.Request) error {
	ss.request = rq
	return nil
}

func (ss *SimpleStorage) GetEmployee(eid int) (*b.Employee, error) {
	if eid == 7 {
		return nil, errors.New("GetEmployee: 7 employee")
	}
	if ss.employee == nil {
		return nil, errors.New("GetEmployee: nil employee")
	}
	return ss.employee, nil
}

// Any possible man with Johnson name is presented in the system and nobody other
type SimpleJohnsonClientSystem struct{}

func (scs *SimpleJohnsonClientSystem) GetClientByName(fName, lName string) *b.Person {
	if fName != "Johnson" {
		return nil
	}

	return b.NewClient(fName, lName)
}

type SimpleJohnsonEmployeeSystem struct{}

func (ses *SimpleJohnsonEmployeeSystem) getSupportEmployees() []int {
	return []int{1, 2}
}

func (ses *SimpleJohnsonEmployeeSystem) getSupportEmployeeByID(int) *PersonDTO {
	return nil
}

func (ses *SimpleJohnsonEmployeeSystem) GetEmployeeID(fName, sName string) int {
	if fName != "Johnson" {
		return -1
	}
	return 1
}

func TestAutoCreateRequest(t *t.T) {
	assert := assert.New(t)
	require := require.New(t)

	storage := &SimpleStorage{}
	cntr := Controller{storage, &SimpleJohnsonEmployeeSystem{}, &SimpleJohnsonClientSystem{}}

	rq_id, err := cntr.AutoCreateRequest("Zorz", PersonDTO{"John", "Galt", b.Client})
	assert.Equal(-1, rq_id)
	assert.NotNil(err)

	rq_id, err = cntr.AutoCreateRequest("Zorz", PersonDTO{"Johnson", "Galt", b.Client})
	assert.Equal(-1, rq_id)
	assert.NotNil(err)

	emp, err := b.NewEmployee("Jack", "Daniels", b.SupportEmployee)
	require.Nil(err)
	require.NotNil(emp)
	storage.SetLbsEmployee(emp)

	rq_id, err = cntr.AutoCreateRequest("Zorz", PersonDTO{"Johnson", "Galt", b.Client})
	assert.Equal(1, rq_id)
	assert.Nil(err)
}

func TestGetOpenRequests(t *t.T) {
	assert := assert.New(t)
	require := require.New(t)

	storage := &SimpleStorage{}
	cntr := Controller{storage, &SimpleJohnsonEmployeeSystem{}, &SimpleJohnsonClientSystem{}}

	rqs, err := cntr.GetOpenRequests("Not Johnson", "Zorz")
	assert.NotNil(err)
	assert.Nil(rqs)

	storage.Err = true
	rqs, err = cntr.GetOpenRequests("Johnson", "Zorz")
	require.NotNil(err)
	require.Nil(rqs)

	storage.Err = false

	tm := time.Date(2007, time.November, 10, 23, 0, 0, 0, time.UTC)

	se, err := b.NewEmployee("Jack", "Daniels", b.SupportEmployee)
	require.Nil(err)
	ca, err := b.NewClientApplication(b.NewClient("Just", "Client"), "zorz", tm)
	require.Nil(err)
	rq, err := b.NewRequest(1, *ca, se)
	require.Nil(err)


	emp, err := b.NewEmployee("Johnson", "Zorz", b.SupportEmployee)
	require.Nil(err)
	require.NotNil(emp)
	storage.SetEmployee(emp)

	reqList := []*b.Request{rq}
	storage.AddEmployeeOpenRequest(rq)
	rqs, err = cntr.GetOpenRequests("Johnson", "Zorz")
	require.Nil(err)
	require.NotNil(rqs)
	assert.Equal(rqs, reqList)
}

func TestCloseRequest(t *t.T) {
	assert := assert.New(t)

	storage := &SimpleStorage{}
	cntr := Controller{storage, &SimpleJohnsonEmployeeSystem{}, &SimpleJohnsonClientSystem{}}

	err := cntr.CloseRequestWithSuccess(1, "Zorz")
	assert.Nil(err)
	err = cntr.CloseRequestWithSuccess(7, "Zorz")
	assert.NotNil(err)

	storage.RemoveRequest()
	err = cntr.CloseRequestWithError(1, "Zorz")
	assert.Nil(err)
	err = cntr.CloseRequestWithError(7, "Zorz")
	assert.NotNil(err)
}

func TestRedirection(t *t.T) {
	assert := assert.New(t)
	require := require.New(t)

	storage := &SimpleStorage{}
	cntr := Controller{storage, &SimpleJohnsonEmployeeSystem{}, &SimpleJohnsonClientSystem{}}

	// Check if Request does not exist
	err := cntr.CloseRedirection(7, "Zorz")
	assert.NotNil(err)
	err = cntr.RedirectRequest(7, "John", "Zorz", "Msg Zorz")
	assert.NotNil(err)

	// check if employee does not exist
	err = cntr.RedirectRequest(1, "Jack", "Zorz", "Msg Zorz")
	assert.NotNil(err)

	emp1, err1 := b.NewEmployee("Johnson", "Zorz", b.BankEmployee)
	emp2, err2 := b.NewEmployee("Johnson", "Zorz", b.SupportEmployee)
	require.Nil(err1)
	require.NotNil(emp1)
	require.Nil(err2)
	require.NotNil(emp2)

	// wrong employee type
	storage.SetEmployee(emp2) // b.SupportEmployee
	err = cntr.RedirectRequest(1, "Johnson", "Zorz", "Msg Zorz")
	assert.NotNil(err)

	// close redirection from not redirected request
	err = cntr.CloseRedirection(1, "Zorz")
	assert.NotNil(err)

	// correct employee type
	storage.SetEmployee(emp1)
	err = cntr.RedirectRequest(1, "Johnson", "Zorz", "Msg Zorz")
	assert.Nil(err)

	err = cntr.CloseRedirection(1, "Zorz")
	assert.Nil(err)
}
