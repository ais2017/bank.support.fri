# Описание организации
Организация предоставляет финансовые услуги физическим и юридическим лицам. Среди услуг: потребительское кредитование, кредитование бизнеса, обслуживание вкладов. Деятельность банка регулируется центральным банком Российской Федерации и ограничивается рядом законов. Клиенты могут обслуживаться в любом из множества имеющихся отделений.
# Описание области автоматизации
Система должна помогать сотрудникам службы поддержки принимать обращения от клиентов по различным вопросам.